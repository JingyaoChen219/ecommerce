/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dbConnection;

/**
 *
 * @author Your Name <Jingyao Chen>
 */
@WebServlet(name = "verfyAccount", urlPatterns = {"/verfyAccount"})
public class verfyAccountServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet verfyAccount</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet verfyAccount at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<String> data = new ArrayList();
        String username = null;
        String password = null;
        String secondPart = new String();
        boolean sucess = false;

        if (request.getParameter("username") != null) {
            username = request.getParameter("username");

        }
        if (request.getParameter("password") != null) {
            password = request.getParameter("password");
        }
        secondPart = String.format("\"%s\"", username);
        String verfyAccountSQL = "SELECT* FROM RegisteredAccount where id_customer=" + secondPart;

        dbConnection connection = new dbConnection();

        try {
            data = connection.verfyAccount(username, password, verfyAccountSQL);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(verfyAccountServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(verfyAccountServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(verfyAccountServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(verfyAccountServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (!data.get(0).endsWith("there is error")) {
            if (data.get(0).endsWith(username) && data.get(1).endsWith(password)) {

                request.setAttribute("username", username);
                request.getRequestDispatcher("homePage.jsp").forward(request, response);
                // response.sendRedirect("homePage.jsp");

            }
        } else {

            PrintWriter out2 = response.getWriter();

            out2.println("<!DOCTYPE html>");
            out2.println("<html>");
            out2.println("<head>");
            out2.println("<title>Servlet verfyAccount</title>");
            out2.println("</head>");
            out2.println("<body>");
            out2.println("<h1> WRONG PASSWORD OR USERNAME " + "</h1>");
            out2.println("</body>");
            out2.println("</html>");

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
