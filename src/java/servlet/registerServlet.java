/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dbConnection;

/**
 *
 * @author Your Name <Jingyao Chen>
 */
@WebServlet(name = "registerServlet", urlPatterns = {"/registerServlet"})
public class registerServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet registerServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet registerServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int reuslt = 0;
        if (request.getParameter("completeregisterButton") != null) {

            String lastN = null;
            String fristN = null;
            String address = null;
            String email = null;
            String phone = null;
            String id_customer = null;
            String password = null;
            String card_type = null;
            String card_exprireDay = null;
            String card_number = null;
            int id_order = 0;

            if (request.getParameter("creditType") != null) {

                card_type = request.getParameter("creditType");
            }

            card_exprireDay = request.getParameter("mm") + request.getParameter("dd") + request.getParameter("yyyy");

            if (request.getParameter("cardNumber") != null) {

                card_number = request.getParameter("cardNumber");
            }

            if (request.getParameter("pin") != null) {

                password = request.getParameter("pin");
            }

            if (request.getParameter("uid") != null) {

                id_customer = request.getParameter("uid");
            }
            if (request.getParameter("lname") != null) {

                lastN = request.getParameter("lname");
            }

            if (request.getParameter("fname") != null) {
                fristN = request.getParameter("fname");

            }

            if (request.getParameter("email") != null) {
                email = request.getParameter("email");

            }
            if (request.getParameter("phone") != null) {
                phone = request.getParameter("phone");

            }

            // && request.getParameter("city") != null && request.getParameter("zip") != null && request.getParameter("state") != null && request.getParameter("country") != null
            if (request.getParameter("address") != null) {

                address = request.getParameter("address") + request.getParameter("city") + request.getParameter("zip") + request.getParameter("state");

            }

            dbConnection connection = new dbConnection();

            try {
                connection.insertAccount(id_customer, id_customer, password);
                out.println("insertAccount successfully");

            } catch (SQLException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

       
            try {
                connection.insertPayment(card_type, card_number, card_exprireDay, id_customer, id_order);
            } catch (SQLException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
                out.println("insertPayment successfully");
           
        
            try {

                if (connection.insertCustomer(id_customer, lastN, fristN, address, email, phone) == 1) {
                    reuslt = 1;
                    out.println("insertcustomer successfully");
                    response.sendRedirect("registerSuccessfully.jsp");

                }

            } catch (SQLException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(registerServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (reuslt == 0) {
                response.sendRedirect("registerError.jsp");

            }

        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void response(HttpServletResponse response, String text) throws IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet registerServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> =   " + text + " </h1>");
            out.println("</body>");
            out.println("</html>");

        }

    }
}
