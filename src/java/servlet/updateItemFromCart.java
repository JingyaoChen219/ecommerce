/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dbConnection;

/**
 *
 * @author Your Name <Jingyao Chen>
 */
@WebServlet(name = "updateItemFromCart", urlPatterns = {"/updateItemFromCart"})
public class updateItemFromCart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet updateItemFromCart</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet updateItemFromCart at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Enumeration<String> aaa;
        String type = null;
        type = request.getParameterNames().nextElement();
        String customer = request.getParameter("customer").toString();
        String qt_str = request.getParameter("qt");
        int qt = Integer.parseInt(qt_str);
        dbConnection connection = new dbConnection();

        try {
            connection.updataCart(qt, type, customer);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(updateItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(updateItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(updateItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(updateItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect("shoppingCart.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Enumeration<String> aaa;
        String type = null;
        type = request.getParameterNames().nextElement();
        String customer = request.getParameter("customer").toString();
        String qt_str = request.getParameter("qt");
        int qt = Integer.parseInt(qt_str);
        dbConnection connection = new dbConnection();

        try {
            connection.deleteShoppingCart(type, customer);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(removeItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(removeItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(removeItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(removeItemFromCart.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("shoppingCart.jsp");

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
