/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dbConnection;

/**
 *
 * @author Your Name <Jingyao Chen>
 */
@WebServlet(name = "addToCartServlet", urlPatterns = {"/addToCartServlet"})
public class addToCartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet addToCartServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet addToCartServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String type = null;
        String company = null;
        double price = 0.00;
        String quantity = null;
        String customer_id = null;
        int insertResult = 0;

        if (request.getParameter("shakeshake") != null) {
            customer_id = request.getParameter("userId");
            type = request.getParameter("shakeshakeType");
            company = request.getParameter("shakeshakeCompany");
            price = Double.parseDouble(request.getParameter("shakeshakePrice"));
            quantity = request.getParameter("shakeshakequantity");
            System.out.println(customer_id + type + company + price + quantity);
            dbConnection connection = new dbConnection();
            try {
                insertResult = connection.insertCart(customer_id, quantity, price, company, type);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (insertResult == 1) {

                String successAddtoArt = "have already add into Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);
            } else {

                String successAddtoArt = "you have already had one  in your Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);

            }

        }

        if (request.getParameter("kfc") != null) {
            customer_id = request.getParameter("userId");
            type = request.getParameter("kfcType");
            company = request.getParameter("kfcCompany");
            price = Double.parseDouble(request.getParameter("kfcPrice"));
            quantity = request.getParameter("kfcquantity");
            System.out.println(customer_id + type + company + price + quantity);
            dbConnection connection = new dbConnection();
            try {
                insertResult = connection.insertCart(customer_id, quantity, price, company, type);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (insertResult == 1) {

                String successAddtoArt = "have already add into Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);
            } else {

                String successAddtoArt = "you have already had one  in your Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);

            }

        }

        if (request.getParameter("chip") != null) {
            customer_id = request.getParameter("userId");
            type = request.getParameter("chipType");
            company = request.getParameter("chipCompany");
            price = Double.parseDouble(request.getParameter("chipPrice"));
            quantity = request.getParameter("chipquantity");
            System.out.println(customer_id + type + company + price + quantity);
            dbConnection connection = new dbConnection();
            try {
                insertResult = connection.insertCart(customer_id, quantity, price, company, type);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (insertResult == 1) {

                String successAddtoArt = "have already add into Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);
            } else {

                String successAddtoArt = "you have already had one  in your Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);

            }

        }

        if (request.getParameter("friday") != null) {
            customer_id = request.getParameter("userId");
            type = request.getParameter("fridayType");
            company = request.getParameter("fridayCompany");
            price = Double.parseDouble(request.getParameter("fridayPrice"));
            quantity = request.getParameter("fridayquantity");
            System.out.println(customer_id + type + company + price + quantity);
            dbConnection connection = new dbConnection();
            try {
                insertResult = connection.insertCart(customer_id, quantity, price, company, type);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (insertResult == 1) {

                String successAddtoArt = "have already add into Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);
            } else {

                String successAddtoArt = "you have already had one  in your Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);

            }

        }

        ///////
        if (request.getParameter("lobster") != null) {
            customer_id = request.getParameter("userId");
            type = request.getParameter("lobsterType");
            company = request.getParameter("lobsterCompany");
            price = Double.parseDouble(request.getParameter("lobsterPrice"));
            quantity = request.getParameter("lobsterquantity");
            System.out.println(customer_id + type + company + price + quantity);
            dbConnection connection = new dbConnection();
            try {
                insertResult = connection.insertCart(customer_id, quantity, price, company, type);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (insertResult == 1) {

                String successAddtoArt = "have already add into Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);
            } else {

                String successAddtoArt = "you have already had one  in your Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);

            }

        }

        ///
        if (request.getParameter("pizza") != null) {
            customer_id = request.getParameter("userId");
            type = request.getParameter("pizzaType");
            company = request.getParameter("pizzaCompany");
            price = Double.parseDouble(request.getParameter("pizzaPrice"));
            quantity = request.getParameter("pizzaquantity");
            System.out.println(customer_id + type + company + price + quantity);
            dbConnection connection = new dbConnection();
            try {
                insertResult = connection.insertCart(customer_id, quantity, price, company, type);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(addToCartServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (insertResult == 1) {

                String successAddtoArt = "have already add into Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);
            } else {

                String successAddtoArt = "you have already had one  in your Cart";
                request.setAttribute("successMessage", successAddtoArt);
                request.getRequestDispatcher("allFoods.jsp").forward(request, response);

            }

        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
