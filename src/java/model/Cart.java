/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author haiqiwu
 */
public class Cart 
{
    int id_customer;
    int id_item;
    int quantity_Of_Item;
    double price_Per_Item;
    double total_Price;
    int id_seller;

    public Cart(int id_customer, int id_item, int quantity_Of_Item, double price_Per_Item, double total_Price, int id_seller) {
        this.id_customer = id_customer;
        this.id_item = id_item;
        this.quantity_Of_Item = quantity_Of_Item;
        this.price_Per_Item = price_Per_Item;
        this.total_Price = total_Price;
        this.id_seller = id_seller;
    }

    public int getId_customer() {
        return id_customer;
    }

    public int getId_item() {
        return id_item;
    }

    public int getQuantity_Of_Item() {
        return quantity_Of_Item;
    }

    public double getPrice_Per_Item() {
        return price_Per_Item;
    }

    public double getTotal_Price() {
        return total_Price;
    }

    public int getId_seller() {
        return id_seller;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public void setId_item(int id_item) {
        this.id_item = id_item;
    }

    public void setQuantity_Of_Item(int quantity_Of_Item) {
        this.quantity_Of_Item = quantity_Of_Item;
    }

    public void setPrice_Per_Item(double price_Per_Item) {
        this.price_Per_Item = price_Per_Item;
    }

    public void setTotal_Price(double total_Price) {
        this.total_Price = total_Price;
    }

    public void setId_seller(int id_seller) {
        this.id_seller = id_seller;
    }
    
    
    
    
    
    
    
    
}
