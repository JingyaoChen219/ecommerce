/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author haiqiwu
 */
public class Order 
{
    int id_order;
    int id_customer;
    String orderDate;
    String orderStatus;
    String shippingNumber;
    String paymentNumber;

    public Order(int id_order, int id_customer, String orderDate, String orderStatus, String shippingNumber, String paymentNumber) {
        this.id_order = id_order;
        this.id_customer = id_customer;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
        this.shippingNumber = shippingNumber;
        this.paymentNumber = paymentNumber;
    }

    public int getId_order() {
        return id_order;
    }

    public int getId_customer() {
        return id_customer;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getShippingNumber() {
        return shippingNumber;
    }

    public String getPaymentNumber() {
        return paymentNumber;
    }

    public void setId_order(int id_order) {
        this.id_order = id_order;
    }

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setShippingNumber(String shippingNumber) {
        this.shippingNumber = shippingNumber;
    }

    public void setPaymentNumber(String paymentNumber) {
        this.paymentNumber = paymentNumber;
    }
    
    
    
}
