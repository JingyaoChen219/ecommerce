/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Your Name <Jingyao Chen>
 */
public class dbConnection {

    String URL = "jdbc:mysql://localhost:3306/chenjing_eCommerce";
    String userName = "root";
    String password = "zzzz";

    Connection connection = null;
    PreparedStatement selectedStatement = null;
    PreparedStatement insertActor;
    PreparedStatement checkAccountActor = null;
    PreparedStatement statement = null;
    ResultSet result = null;
    int insertResult;

    // FIRST THING TO DO ---- CONNECTEING TO DATABASE
    public Connection connectToDB() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();

            connection = DriverManager.getConnection(URL, userName, password);
            if (!connection.isClosed()) {
                out.println("Have successfully connected to DataBase");
                //  connection.close();

            }

        } catch (SQLException e) {
            out.println("Unable to connect to database");
            e.printStackTrace();
        }
        return connection;

    }

    /*
    @author jingyao chen
    
    // add into customer table
     */
    public int insertCustomer(String id_customer, String lastName,
            String firstName, String address, String email, String phone
    ) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;

        Connection conn;
        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO Customer ( id_customer,  lastName, firstName,  address,  email,  phone)" + "VALUES(?,?,?,?,?,?)");

        insertActor.setString(1, id_customer);
        insertActor.setString(2, lastName);
        insertActor.setString(3, firstName);
        insertActor.setString(4, address);
        insertActor.setString(5, email);
        insertActor.setString(6, phone);

        insertResult = insertActor.executeUpdate();
        return insertResult;

    }

    /*
    @author jingyao chen
    
    // add into account table
     */
    public int insertAccount(String id_customer, String username,
            String password
    ) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;

        Connection conn;
        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO RegisteredAccount ( id_customer,  userName, password)" + "VALUES(?,?,?)");

        insertActor.setString(1, id_customer);
        insertActor.setString(2, username);
        insertActor.setString(3, password);

        insertResult = insertActor.executeUpdate();
        return insertResult;

    }


    /*
    @author jingyao chen
    
    // add into payment table
     */
    public void insertPayment(String pType, String cardNum, String cardED, String id_customer, int id_order)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;
        Connection conn;

        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO Payment ( paymentType, cardNumber, cardExprireDate, id_customer, id_order)" + "VALUES(?,?,?,?,?)");

        insertActor.setString(1, pType);
        insertActor.setString(2, cardNum);
        insertActor.setString(3, cardED);
        insertActor.setString(4, id_customer);
        insertActor.setInt(5, id_order);

        insertResult = insertActor.executeUpdate();
    }

    /*
    @author jingyao chen
    
    // check data from account table 
    // check if the passwork is correct 
     */
    public ArrayList<String> verfyAccount(String username, String password, String verfyAccountSQL) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        ArrayList<String> metaData = new ArrayList();
        Connection conn;
        conn = this.connectToDB();
        checkAccountActor = conn.prepareStatement(verfyAccountSQL);

        result = checkAccountActor.executeQuery(verfyAccountSQL);
        if (result.next()) {
            String name = result.getString(2);

            String pin = result.getString(3);
            metaData.add(name);
            metaData.add(pin);

        } else {
            metaData.add("there is error");

        }

        return metaData;

    }

    public int insertCart(String customer_id, String quantity, double price, String company, String type) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        insertResult = 0;
        Connection conn;
        conn = this.connectToDB();
        insertActor = conn.prepareStatement("INSERT INTO Cart (id_customer, quantity, price, id_company,item_type)" + "VALUES(?,?,?,?,?)");
        insertActor.setString(1, customer_id);
        insertActor.setString(2, quantity);
        insertActor.setDouble(3, price);
        insertActor.setString(4, company);
        insertActor.setString(5, type);

        insertResult = insertActor.executeUpdate();
        return insertResult;
    }

    public int insertItem(int id_item, String type, double price, int id_seller) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        Connection conn;
        conn = this.connectToDB();
        insertActor = conn.prepareStatement("INSERT INTO Item (id_item, type, price, id_seller)" + "VALUES(?,?,?,?)");
        insertActor.setInt(1, id_item);
        insertActor.setString(2, type);
        insertActor.setDouble(3, price);
        insertActor.setInt(4, id_seller);
        insertResult = insertActor.executeUpdate();
        return insertResult;
    }

    public void insertInventory(int id_item, String itemName,
            String quantity, String price, int id_seller
    ) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;
        Connection conn;
        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO Inventory ( id_item,  itemName, quantity,  price,  id_seller)" + "VALUES(?,?,?,?,?)");

        insertActor.setInt(1, id_item);
        insertActor.setString(2, itemName);
        insertActor.setString(3, quantity);
        insertActor.setString(4, price);
        insertActor.setInt(5, id_seller);

        insertResult = insertActor.executeUpdate();
    }

    public int insertOrder(int ord, String cus, String da, int to)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        int insertResult = 0;
        int orde = ord;
        String custer = cus.trim();
        String dat = da.trim();
        int tot = to;
        String w = "\"";

        String sqlstatement = "INSERT INTO OrderDetails VALUES ( " + orde + "," + w + custer + w + "," + w + dat + w + "," + tot + ")";
        System.out.println(sqlstatement);

        Connection conn;

        conn = this.connectToDB();

        insertActor = conn.prepareStatement(sqlstatement);

        insertResult = insertActor.executeUpdate();
        System.out.println("fdafd" + insertResult);
        return insertResult;

    }

    public int deleteShoppingCart(String type, String customer) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {

        int insertResult = 0;
        Connection conn;
        String firstPart = customer.toString();
        String secondPart = type.toString();
        conn = this.connectToDB();

        String sqlStatement = "DELETE FROM Cart where id_customer = " + "\"" + firstPart + "\"" + "and item_type = " + "\"" + secondPart + "\"";;

        insertActor = conn.prepareStatement(sqlStatement);
        insertResult = insertActor.executeUpdate();
        return insertResult;

    }

    public int updataCart(int qt, String type, String customer) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {

        int insertResult = 0;
        Connection conn;
        String firstPart = customer.toString();
        String secondPart = type.toString();
        conn = this.connectToDB();
        String wtf = "\"";
        String sqlStatement = " update Cart set quantity=" + wtf + qt + wtf + "where id_customer = " + wtf + firstPart + wtf + "and item_type = " + wtf + secondPart + wtf;
        insertActor = conn.prepareStatement(sqlStatement);
        insertResult = insertActor.executeUpdate();
        return insertResult;

    }

    public void insertOrderDetails(int id_order, int id_item, int qualitity, double unitprice, double discount, String orderNumber)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        int insertResult = 0;
        Connection conn;

        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO OrderDetails ( id_order, id_item, qualitity, unitprice, discount,  orderNumber  )" + "VALUES(?,?,?,?,?,?)");

        insertActor.setInt(1, id_order);
        insertActor.setInt(2, id_item);
        insertActor.setInt(3, qualitity);
        insertActor.setDouble(4, unitprice);
        insertActor.setDouble(5, discount);
        insertActor.setString(6, orderNumber);

        insertResult = insertActor.executeUpdate();
    }

    public void insertReview(int id_item, int id_seller, int id_customer, String rating, String detailedReview, String Reviewcol)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;
        Connection conn;

        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO Review (  id_item  ,  id_seller, id_customer, rating,detailedReview, Reviewcol    )" + "VALUES(?,?,?,?,?,?)");

        insertActor.setInt(1, id_item);
        insertActor.setInt(2, id_seller);
        insertActor.setInt(3, id_customer);
        insertActor.setString(4, rating);
        insertActor.setString(5, detailedReview);
        insertActor.setString(6, Reviewcol);

        insertResult = insertActor.executeUpdate();
    }

    public void insertSeller(int idSeller, String sellerName, String companyName)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;
        Connection conn;

        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO Seller (  idSeller  ,  sellerName, companyName    )" + "VALUES(?,?,?)");

        insertActor.setInt(1, idSeller);
        insertActor.setString(2, sellerName);
        insertActor.setString(3, companyName);

        insertResult = insertActor.executeUpdate();
    }

    public void insertShipment(String shipDetail, String typeShipment, String physicalAddress, String cost, int id_order)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;
        Connection conn;

        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO Shipment (  shipment Detail  ,  typeShipment, physicalAddress, cost, id_order    )" + "VALUES(?,?,?,?,?)");

        insertActor.setString(1, shipDetail);
        insertActor.setString(2, typeShipment);
        insertActor.setString(3, physicalAddress);
        insertActor.setString(4, cost);
        insertActor.setInt(5, id_order);

        insertResult = insertActor.executeUpdate();
    }

    public void insertCustomerRep(int id_customerRep, String username, String password)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int insertResult = 0;
        Connection conn;

        conn = this.connectToDB();

        insertActor = conn.prepareStatement("INSERT INTO customerRep (    id_customerRep   ,  username, password    )" + "VALUES(?,?,?)");

        insertActor.setInt(1, id_customerRep);
        insertActor.setString(2, username);
        insertActor.setString(3, password);

        insertResult = insertActor.executeUpdate();
    }

    //
    //2. select from
    public ArrayList<Item> getAllItems() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int result = 0;
        Connection conn;
        conn = this.connectToDB();

        statement = (PreparedStatement) conn.createStatement();
        ResultSet r = statement.executeQuery("select id_item, type, price, id_seller"
                + " from Item ");

        ArrayList<Item> items = new ArrayList();

        while (r.next()) {
            //int a = r.getInt(1);
            items.add(new Item(r.getInt(1), r.getString(2), r.getDouble(3), r.getInt(4)));
        }
        return items;

    }

    //3. add to cart:
    public void addItemToCart(Item item, int quantity, int id_customer)
            throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int result = 0;
        Connection conn;
        conn = this.connectToDB();
        //1.id_customer, 2. id_item 3. quantity 4. ppi 5.total_Price   6.id_seller
        int id_item = item.getId_item();
        int id_seller = item.getId_seller();
        double ppi = item.getPrice();
        double total_Price = quantity * ppi;

        insertActor = conn.prepareStatement("INSERT INTO Cart (    id_customer   ,  id_item, quantity Of Item, price Per Item"
                + ",total Price, id_seller    )" + "VALUES(?,?,?,?,?,?)");

        insertActor.setInt(1, id_customer);
        insertActor.setInt(2, id_item);
        insertActor.setInt(3, quantity);
        insertActor.setDouble(4, ppi);
        insertActor.setDouble(5, total_Price);
        insertActor.setInt(6, id_seller);

        result = insertActor.executeUpdate();

    }

    //clear one's shopping cart
    public void clearAllItemsInCart(String id_customer) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        int result = 0;
        Connection conn;
        conn = this.connectToDB();
        String secondPart;
        secondPart = String.format("\"%s\"", id_customer);
        String sql = "delete from Cart where id_customer=" + secondPart;
        //Catagory c = new Category();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.executeUpdate();

    }

}
