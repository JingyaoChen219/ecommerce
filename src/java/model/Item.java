/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author haiqiwu
 */
public class Item {
    int id_item;
    String type;
    double price;
    int id_seller;

    public Item(int id_item, String type, double price, int id_seller) {
        this.id_item = id_item;
        this.type = type;
        this.price = price;
        this.id_seller = id_seller;
    }

    public int getId_item() {
        return id_item;
    }

    public String getType() {
        return type;
    }

    public double getPrice() {
        return price;
    }

    public int getId_seller() {
        return id_seller;
    }

    public void setId_item(int id_item) {
        this.id_item = id_item;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setId_seller(int id_seller) {
        this.id_seller = id_seller;
    }
    
    
}
