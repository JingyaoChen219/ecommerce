CREATE DATABASE  IF NOT EXISTS `chenjing_eCommerce` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `chenjing_eCommerce`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: chenjing_eCommerce
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AddItem`
--

DROP TABLE IF EXISTS `AddItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AddItem` (
  `id_item` int(9) NOT NULL DEFAULT '0',
  `id_admin` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_admin`,`id_item`),
  KEY `ToItem` (`id_item`),
  CONSTRAINT `ToAdminManager` FOREIGN KEY (`id_admin`) REFERENCES `AdminManger` (`id_admin`),
  CONSTRAINT `ToItem` FOREIGN KEY (`id_item`) REFERENCES `Item` (`id_item`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AddItem`
--

LOCK TABLES `AddItem` WRITE;
/*!40000 ALTER TABLE `AddItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `AddItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AdminManger`
--

DROP TABLE IF EXISTS `AdminManger`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AdminManger` (
  `id_admin` int(5) NOT NULL,
  `username` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_admin`),
  CONSTRAINT `adminMangerToEmployee` FOREIGN KEY (`id_admin`) REFERENCES `Employee` (`id_employee`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AdminManger`
--

LOCK TABLES `AdminManger` WRITE;
/*!40000 ALTER TABLE `AdminManger` DISABLE KEYS */;
/*!40000 ALTER TABLE `AdminManger` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cart`
--

DROP TABLE IF EXISTS `Cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cart` (
  `id_customer` varchar(45) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `quantity` int(11) DEFAULT '1',
  `price` double DEFAULT NULL,
  `id_company` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `item_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_customer`,`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cart`
--

LOCK TABLES `Cart` WRITE;
/*!40000 ALTER TABLE `Cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `Cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Customer`
--

DROP TABLE IF EXISTS `Customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Customer` (
  `id_customer` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'LASTNAME',
  `firstName` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'FIRSTNAME',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'HOME ADDRESS',
  `email` varchar(30) COLLATE utf8_unicode_ci DEFAULT 'EMAIL',
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT 'PHONE',
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Customer`
--

LOCK TABLES `Customer` WRITE;
/*!40000 ALTER TABLE `Customer` DISABLE KEYS */;
INSERT INTO `Customer` VALUES ('cse305','yang','terry','140-60 ash ave 6jlake grove11755ny','jingyaochen@ymail.com','9172384393');
/*!40000 ALTER TABLE `Customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Employee`
--

DROP TABLE IF EXISTS `Employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Employee` (
  `id_employee` int(5) NOT NULL,
  `role` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `dataJoined` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `id_supervisor` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Employee`
--

LOCK TABLES `Employee` WRITE;
/*!40000 ALTER TABLE `Employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `Employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Inventory`
--

DROP TABLE IF EXISTS `Inventory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Inventory` (
  `id_item` int(14) NOT NULL,
  `itemName` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `quantity` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `price` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `id_seller` int(9) NOT NULL,
  PRIMARY KEY (`id_item`,`id_seller`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Inventory`
--

LOCK TABLES `Inventory` WRITE;
/*!40000 ALTER TABLE `Inventory` DISABLE KEYS */;
/*!40000 ALTER TABLE `Inventory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Item`
--

DROP TABLE IF EXISTS `Item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Item` (
  `id_item` int(14) NOT NULL,
  `type` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `price` double DEFAULT NULL,
  `id_seller` int(15) NOT NULL,
  PRIMARY KEY (`id_item`,`id_seller`),
  KEY `id_item` (`id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Item`
--

LOCK TABLES `Item` WRITE;
/*!40000 ALTER TABLE `Item` DISABLE KEYS */;
/*!40000 ALTER TABLE `Item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order`
--

DROP TABLE IF EXISTS `Order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Order` (
  `order` int(11) NOT NULL,
  `customer` varchar(45) NOT NULL,
  `data` varchar(45) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`customer`,`order`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order`
--

LOCK TABLES `Order` WRITE;
/*!40000 ALTER TABLE `Order` DISABLE KEYS */;
/*!40000 ALTER TABLE `Order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderDetails`
--

DROP TABLE IF EXISTS `OrderDetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderDetails` (
  `order` int(11) NOT NULL,
  `customer` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `data` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  PRIMARY KEY (`order`,`customer`),
  KEY `id_order` (`order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderDetails`
--

LOCK TABLES `OrderDetails` WRITE;
/*!40000 ALTER TABLE `OrderDetails` DISABLE KEYS */;
INSERT INTO `OrderDetails` VALUES (234432,'cse305','12/2017',179),(299700,'cse305','12/2017',120);
/*!40000 ALTER TABLE `OrderDetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payment`
--

DROP TABLE IF EXISTS `Payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Payment` (
  `paymentType` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardNumber` varchar(45) CHARACTER SET utf8 NOT NULL,
  `cardExprireDate` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `id_customer` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_customer`,`cardNumber`),
  KEY `id_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payment`
--

LOCK TABLES `Payment` WRITE;
/*!40000 ALTER TABLE `Payment` DISABLE KEYS */;
INSERT INTO `Payment` VALUES ('visa','135847565838','may42015','cse305',0);
/*!40000 ALTER TABLE `Payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RegisteredAccount`
--

DROP TABLE IF EXISTS `RegisteredAccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RegisteredAccount` (
  `id_customer` varchar(45) NOT NULL,
  `userName` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RegisteredAccount`
--

LOCK TABLES `RegisteredAccount` WRITE;
/*!40000 ALTER TABLE `RegisteredAccount` DISABLE KEYS */;
INSERT INTO `RegisteredAccount` VALUES ('cse305','cse305','aaaaaa');
/*!40000 ALTER TABLE `RegisteredAccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Review`
--

DROP TABLE IF EXISTS `Review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Review` (
  `id_item` int(14) NOT NULL,
  `id_seller` int(9) NOT NULL,
  `id_customer` int(9) NOT NULL,
  `rating` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `detailedReview` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Reviewcol` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_item`,`id_seller`,`id_customer`),
  KEY `reviewToCoustimer_idx` (`id_customer`),
  KEY `reviewToSeller_idx` (`id_seller`),
  KEY `id_item` (`id_item`),
  CONSTRAINT `reviewToSeller` FOREIGN KEY (`id_seller`) REFERENCES `Seller` (`idSeller`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Review`
--

LOCK TABLES `Review` WRITE;
/*!40000 ALTER TABLE `Review` DISABLE KEYS */;
/*!40000 ALTER TABLE `Review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Seller`
--

DROP TABLE IF EXISTS `Seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Seller` (
  `idSeller` int(9) NOT NULL,
  `sellerName` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `companyName` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`idSeller`),
  KEY `SellerToReview_idx` (`idSeller`,`sellerName`),
  CONSTRAINT `SellerToReview` FOREIGN KEY (`idSeller`) REFERENCES `Review` (`id_seller`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Seller`
--

LOCK TABLES `Seller` WRITE;
/*!40000 ALTER TABLE `Seller` DISABLE KEYS */;
/*!40000 ALTER TABLE `Seller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Shipment`
--

DROP TABLE IF EXISTS `Shipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Shipment` (
  `shipment Detail` int(11) DEFAULT NULL COMMENT 'By USPS/FEDEX/UPS',
  `typeShipment` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `physicalAddress` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `cost` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `id_order` int(11) NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Shipment`
--

LOCK TABLES `Shipment` WRITE;
/*!40000 ALTER TABLE `Shipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `Shipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerRep`
--

DROP TABLE IF EXISTS `customerRep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerRep` (
  `id_customerRep` int(5) NOT NULL,
  `username` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id_customerRep`),
  CONSTRAINT `customerRepoToEmployee` FOREIGN KEY (`id_customerRep`) REFERENCES `Employee` (`id_employee`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerRep`
--

LOCK TABLES `customerRep` WRITE;
/*!40000 ALTER TABLE `customerRep` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerRep` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 23:31:38
