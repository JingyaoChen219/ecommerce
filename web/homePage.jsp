<%-- 
    Document   : homePage
    Created on : Dec 4, 2017, 11:11:33 PM
    Author     : Your Name <Jingyao Chen>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- bootstrap--%>
<!--<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/homePageStyle.css">
        <title>Home Page</title>
    </head>

    <body style="background-color:">
    <center>
        <img src="image/header.jpeg" width="1500" height="100" alt="header"/>
        <jsp:include flush="true" page="navBar.jsp"></jsp:include>
            <h1>
            <%
                String username = null;
                String passUserName = null;
                if (request.getAttribute("username") != null) {
                    username = request.getAttribute("username").toString();
                    out.print("Welcome:" + request.getAttribute("username").toString());

                }
                session.setAttribute("user", request.getAttribute("username"));

            %>

        </h1>

        <br/>
        <br/>


        <table  width="1000"  align="center">

            <thead>
                <tr>
                    <th> <div class="fast"> <h2 align="center">Fast Food</h2> </div></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center"><img src="image/catg_fastfood.jpg" width="1000" height="400" alt="catg_fastfood"/>
                    </td>
                </tr>

            </tbody>
        </table>
        <br/>
        <br/>

        <table  width="1000"  align="center">
            <thead>
                <tr>
                    <th> <div class="chinese"> <h2 align="center">Chinese Food</h2> </div></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center"><img src="image/catg_chinesefood.jpg" width="1000" height="400" alt="catg_chinesefood"/>
                    </td>
                </tr>

            </tbody>
        </table>
        <br/>
        <br/>

        <table  width="1000"  align="center">
            <thead>
                <tr>
                    <th><div class="indian"> <h2 align="center">Indian Food</h2> </div></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center"><img src="image/catg_indianfood.jpg" width="1000" height="400" alt="catg_indianfood"/>
                    </td>
                </tr>

            </tbody>
        </table>
        <br/>
        <br/>

        <table  width="1000"  align="center">
            <thead>
                <tr>
                    <th><div class="vege"> <h2 align="center">Vegetarian</h2> </div></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center"><img src="image/catg_vegefood.jpg" width="1000" height="400" alt="catg_vegefood"/>
                    </td>
                </tr>

            </tbody>
        </table>
        <br/>
        <br/>
        <br/>

        <table width="1200" align="center">
            <thead>
                <tr>
                    <td>
                        <img src="image/catg_appdownload_meitu_2.jpg" width="1000" height="300" alt="catg_appdownload_meitu_2" />
                    </td>
                </tr>
            </thead>
        </table>
        <br/>
        <br/>
        <br/>
        <jsp:include flush="true" page="footer.jsp"></jsp:include>
    </center>

</body>
</html>
