<%-- 
    Document   : allProducts
    Created on : Dec 6, 2017, 3:45:10 AM
    Author     : Your Name <Jingyao Chen>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- bootstrap--%>
        <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
        <title>All Foods</title>
    </head>
    <body>

    <center style="background-color:">
        <img src="image/header.jpeg" width="1500" height="100" alt="header"/>
        <jsp:include flush="true" page="navBar.jsp"></jsp:include>
            <center>

                <p>Message: ${successMessage}</p>

            <form action="addToCartServlet" method="POST">
                <h1>
                    <%

                        if (session.getAttribute("user") != null) {

                            out.print("Welcome:" + session.getAttribute("user"));

                        }


                    %>
                </h1>

                <input type="hidden" name="userId" value="<%= session.getAttribute("user")%>"  />


                <table border="0" width="1500" height="400" cellspacing="0" cellpadding="0">

                    <tbody>


                    <thead>

                    <td>Picture</td>
                    <td>Kind</td>
                    <td>Company</td>
                    <td>Price</td>
                    <td>Quantity</td>
                    <td>Get Stuffed</td>


                    </thead>


                    <tr>

                        <td><img src="image/shake_shack_burgers.jpg" width="150" height="150" alt="shake_shack_burgers"/></td>
                        <td><input type="text"  name="shakeshakeType" value="Burger" /></td>
                        <td><input type="text"  name="shakeshakeCompany" value="Shake&Shake" /></td>
                        <td><input type="text"  name="shakeshakePrice" value="8.99" /></td>
                        <td><input type="text" name="shakeshakequantity" value="1" /></td>
                        <td><input type="submit" value="Add To Cart" name="shakeshake" /></td>
                    </tr>


                    <tr>
                        <td><img src="foodImage/kfcMealBox.jpg" width="150" height="150" alt="kfcMealBox"/></td>
                        <td><input type="text"  name="kfcType" value="Box_Meal"/></td>
                        <td><input type="text"  name="kfcCompany"value="KFC" /></td>
                        <td><input type="text"  name="kfcPrice"value="7.99" /></td>
                        <td><input type="text"  name="kfcquantity" value="1" /></td>
                        <td><input type="submit"name="kfc" value="Add To Cart"  /></td>
                    </tr>


                    <tr>
                        <td><img src="image/chipotle-chocolate-chili-final.jpg" width="150" height="150" alt="chipotle-chocolate-chili-final"/>
                        </td>


                        <td><input type="text"  name="chipType" value="Chocolate_Chili"/></td>
                        <td><input type="text"  name="chipCompany"value="Chipotle" /></td>
                        <td><input type="text"  name="chipPrice"value="11.99" /></td>
                        <td><input type="text"  name="chipquantity" value="1" /></td>
                        <td><input type="submit"name="chip" value="Add To Cart"  /></td>
                    </tr>





                    <tr>
                        <td><img src="image/Friday.jpg" width="150" height="150" alt="Friday"/>
                        </td>


                        <td><input type="text"  name="fridayType" value="Steak"/></td>
                        <td><input type="text"  name="fridayCompany"value="Friday" /></td>
                        <td><input type="text"  name="fridayPrice"value="19.99" /></td>
                        <td><input type="text"  name="fridayquantity" value="1" /></td>
                        <td><input type="submit"name="friday" value="Add To Cart"  /></td>

                    </tr>






                    <tr>
                        <td><img src="image/Red-Lobster-Lobsterfest-20131.jpg" width="150" height="150" alt="Red-Lobster-Lobsterfest-20131"/>
                        </td>

                        <td><input type="text"  name="lobsterType" value="Lobster"/></td>
                        <td><input type="text"  name="lobsterCompany"value="Red Lobster" /></td>
                        <td><input type="text"  name="lobsterPrice"value="23.99" /></td>
                        <td><input type="text"  name="lobsterquantity" value="1" /></td>
                        <td><input type="submit"name="lobster" value="Add To Cart"  /></td>

                    </tr>


                    <tr>
                        <td><img src="foodImage/pizza.jpg" width="150" height="150" alt="pizza"/>
                        </td>



                        <td><input type="text"  name="pizzaType" value="8inch_pizza_with_three_topping"/></td>
                        <td><input type="text"  name="pizzaCompany"value="pizza Hut" /></td>
                        <td><input type="text"  name="pizzaPrice"value="11.99" /></td>
                        <td><input type="text"  name="pizzaquantity" value="1" /></td>
                        <td><input type="submit"name="pizza" value="Add To Cart"  /></td>

                    </tr>

            </form>
            </tbody>
            </table>
        </center>


        <jsp:include flush="true" page="footer.jsp"></jsp:include>
    </center>
</body>
</html>
