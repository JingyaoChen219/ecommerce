<%-- 
    Document   : index
    Created on : Dec 5, 2017, 6:30:30 AM
    Author     : Your Name <Jingyao Chen>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!--<link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>-->

<!DOCTYPE html>
<html>
    <head>
        <title>welcome to Chicken Dinner</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="stylesheet" href="css/indexStyle.css">
        
        <%-- bootstrap--%>
        <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    </head>

    <body style="background-color:">

    <center>
        <div class="welcome">
                <h1  > Welcome to Chicken Dinner </h1>
        </div>
        <img  src="image/logo.jpeg"  width="1000" height="500"  alt="welcome to ChickenDinner" />


        <table border="0" >
            <thead>
                <tr>
                    <th><img src="image/shake_shack_burgers.jpg " width="200" height="200" alt="applebee"/>
                    </th>
                    <th><img src="image/Red-Lobster-Lobsterfest-20131.jpg" width="200" height="200" alt="Red-Lobster-Lobsterfest-20131"/>
                    </th>
                    <th>
                        <img src="image/chipotle-chocolate-chili-final.jpg" width="200" height="200"/>
                    </th>
                    <th>
                        <img src="image/Friday.jpg" width="200" height="200" alt="Friday"/>

                    </th>
                    <th>
                        <img src="image/inAndOut.png" width="200" height="200" alt="inAndOut"/>

                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><img src="image/kfc.png" width="200" height="200" align="center" alt="kfc"/>
                    </td>
                    <td><img src="image/tacoBell.jpg" width="200" height="200" alt="tacoBell"/>
                    </td>
                    <td><img src="image/mc.jpg" width="200" height="200"/>
                    </td>
                    <th>
                        <img src="image/applebee.jpg" width="200" height="200" alt="shake_shack_burgers"/>

                    </th>
                    <th>
                        <img src="image/BRIO Filetto Di Romano.jpg" width="200" height="200" alt="BRIO Filetto Di Romano"/>
                    </th>
                </tr>
            </tbody>
        </table>



<!--        <table border="0" width="90" cellspacing="5" cellpadding="4">
            <thead>
                <tr>
                    <th>
                        <form action="homePage.jsp">
                            <input type="submit" value="Continue" name="continueButton" />

                        </form>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <form action="homePage.jsp">
                            <input type="submit" value="Continue" name="continueButton" />

                        </form>
                    </td>
                </tr> 
                <br/>
                <tr>
                    <td> <form action="login.jsp" >

                            <input type="submit" value="Log in to your Account" name="loginInButton" />

                        </form></td>
                </tr>
                <br/>
                <tr>
                    <td><form action="register.jsp" >

                            <input type="submit" value="Register new Account" name="registerButton" />

                        </form> </td>
                </tr>
            </tbody>
        </table>-->

        <form action="homePage.jsp">
            <input type="submit" value="Continue" name="continueButton" />

        </form>
        <br/>
        <form action="login.jsp" >

            <input type="submit" value="Log in to your Account" name="loginInButton" />

        </form>
        <br/>
        <form action="register.jsp" >

            <input type="submit" value="Register new Account" name="registerButton" />

        </form>


        <jsp:include flush="true" page="footer.jsp"></jsp:include>

    </center>

</body>
</html>
